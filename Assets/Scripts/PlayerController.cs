﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float jump;
    public Text countText;
    public Text winText;
    private Rigidbody rb;
    private int count;
    private bool resetting = false;
    private int resetDelay = 0;
    private GameObject[] pickups;

    void Start()
    {
        pickups = GameObject.FindGameObjectsWithTag("Pick Up");
        rb = GetComponent<Rigidbody>();
        count = 0;
        updateCountText();
        winText.text = "";
    }



    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);

        if (Input.GetKeyDown(KeyCode.Space))
        {
           rb.AddForce(new Vector3(0, jump, 0));
        }

        if (transform.position.y < 0)
        {
            winText.text = "You Lose :P!";
            resetting = true;

        }
        resetCheck();
        teleporterCheck();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);


            count = count + 1;
            updateCountText();
        }
    }

    void updateCountText()
    {
        countText.text = "Count: " + count.ToString() + "/45";
        if (count >= 45)
        {
            winText.text = " ConBRADulations!!";
            resetting = true;
        }
    }
    void teleporterCheck() { 
    

    }
    void resetCheck()
    {
        if (resetting)
        {
            resetDelay++;
            if (resetDelay > 90)
            {
                winText.text = "";
                transform.position = new Vector3(2.3f, 0.5f, 0.1f);
                resetting = false;
                resetDelay = 0;
                count = 0;
                updateCountText();

              

                foreach (GameObject pickup in pickups)
                {
                    pickup.gameObject.SetActive(true);
                }
            }
        }
    }


}