# README #

**This is Lariel's Project**

It's a simple game she's creating in Unity (download from https://store.unity.com/products/unity-personal) that involves rolling around a ball by using the arrow keys and a couple other things.

Main tutorial page: https://unity3d.com/learn/tutorials/projects/roll-ball-tutorial
 - We're on the **Setting Up the Game** section.

YouTube tutorial: (last left off at lesson 7 of 8) https://www.youtube.com/watch?v=bFSLI2cmYYo#t=519.603145